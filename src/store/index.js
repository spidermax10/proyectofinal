import { createStore } from 'vuex'

export default createStore({
  state: {
    Registros:[],
    Registro:{
      id:'',
      NumeroCedula:'',
      Apellidos:'',
      Nombres:'',
      Sexo:'',
      FechaNacimiento:'',
      EstadoCivil:'',
      NombreConyuge:'',
      NumeroHijos:'',
      Ciudad:'',
      Direccion:'',
      Celular:'',
      CorreoElectronico:'',
      Discapacidad:''




  }
  },
  getters: {
  },
  mutations: {
    set(state, payload){
      state.Registros.push(payload)
    }
  },
  actions: {
    async setRegistros({commit}, Registro){
      try {
        const res =  await fetch(`https://registroempleados-6c5b6-default-rtdb.firebaseio.com/registrosmepleados/${Registro.id}.json`, {
          method: 'PUT',
          body: JSON.stringify(Registro),
          headers:{
            'Content-Type': 'application/json'
          }
        })
        const RegistroDB = await res.json()
        console.log(RegistroDB)
        
      } catch (error) {
        console.log(error)
        
      }
      commit('set', Registro)

    }
  },
  modules: {
  }
})
